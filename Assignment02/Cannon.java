import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Cannon. The arrow thing you control.
 * 
 * @author Carl Ådahl
 * @version 2010-10
 */
public class Cannon extends SmoothActor
{
    private boolean waitingForBall = false;

    private static final double ANGULAR_SPEED = 0.9; // how many degrees we turn per 1/100th second.
    private static final double MAX_ANGLE = 80; // how far we can rotate the cannon in either direction.

    private double angle = 0; // current floating-point angle. we want high precision.
    private Ball ball = null; // ball "owned" by the cannon (until it hits the map).
    private boolean wasUpPressed = false; // key-release tracking variable (see below).

    protected void addedToWorld(World world)
    {
        // We're in the world, so prepare the first ball.
        prepareBall();
    }

    public void act(double deltaTime) 
    {
        // Handle key presses.
        handleKeys(deltaTime);

        // If we're waiting for our moving ball to hit something, and it now stopped moving...
        boolean ballFinishedMoving = ball.getState() != Ball.STATE_MOVING;
        if(waitingForBall && ballFinishedMoving)
        {
            // Stop waiting and load up a new ball.
            waitingForBall = false;
            prepareBall();
        }
    }

    public void prepareBall()
    {
        // Get a random ball type from the list of allowed ones. Only balls currently in the map
        // will be in the list.
        int[] allowedBallTypes = ((BubbleWorld)getWorld()).getMap().getAllowedBallTypes();
        
        if (allowedBallTypes.length > 0) {
            int type = allowedBallTypes[Greenfoot.getRandomNumber(allowedBallTypes.length)];
            
            // Create it and add it to the world.
            ball = new Ball(type);
            getWorld().addObject(ball, getX(), getY());
        }
    }

    private void fireBall()
    {
        // Play sound when the ball is fired.
        Greenfoot.playSound("fire.wav");
        
        // Get the ball moving and begin waiting for it to hit something.
        ball.move(angle);
        waitingForBall = true;
    }

    private void handleKeys(double deltaTime)
    {   
        // Player presses the UP arrow: fire a ball if we're ready.
        // Note: The extra code is to ensure that the player releases the key before
        // firing again. Greenfoot lacks a better way to do it.
        boolean isUpPressed = Greenfoot.isKeyDown("up");
        if(isUpPressed && !wasUpPressed && !waitingForBall)
        {
            fireBall();
        }

        // Save the current up-key-pressed state for next time.
        wasUpPressed = isUpPressed;

        // Player presses the LEFT arrow: turn the cannon left.
        if(Greenfoot.isKeyDown("left"))
        {
            angle = Math.max(-MAX_ANGLE, angle - ANGULAR_SPEED * deltaTime);
            setRotation((int)angle);
        }

        // Player presses the RIGHT arrow: turn the cannon right.
        if(Greenfoot.isKeyDown("right"))
        {
            angle = Math.min(MAX_ANGLE, angle + ANGULAR_SPEED * deltaTime);
            setRotation((int)angle);
        }
    }    
}
