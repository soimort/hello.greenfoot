import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A simple effect that is shown when a ball matches.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class StickEffect  extends SmoothActor
{
    double alpha = 255;

    public void act(double deltaTime) 
    {
        alpha -= deltaTime*16.0;

        if(alpha <= 0)
        {
            getWorld().removeObject(this);
            return;
        }
        
        getImage().setTransparency((int)alpha);
    }    
}
