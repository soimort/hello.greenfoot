import greenfoot.*;
import java.util.*;

/**
 * The map, including gameplay logic.
 * 
 * Balls are laid out in a hexagon grid. It works like this using (i,j) coordinates:
 * 
 * (0,0)     (1,0)     (2,0)     (3,0)  ...
 * 
 *      (0,1)     (1,1)     (2,1)   ...
 * 
 * (0,2)     (1,2)     (2,2)     (3,2)  ...
 * 
 * So a hex tile has SIX neighbors. Left, right, top-left, top-right, bottom-left, 
 * and bottom-right. How to find them depends on whether the row is odd or even.
 * 
 * All: Left is (i-1,j). right is (i+1,j).
 * Even rows: Top-left is (i-1,j-1), top-right is (i, j-1).
 * Odd rows: Top-left is (i,j-1), top-right is (i+1,j-1).
 * 
 * Figure out the coordinates, then call getCell(i,j) to grab a cell...
 * 
 * @author Carl Ådahl
 * @version 2010-10
 */
public class Map  
{
    // Small helper class to store i and j positions both in one object.
    public class Position
    {
        public int i, j;
    }

    public static final int MAX_WIDTH = 8;       // maximum width of map in cells
    public static final int MAX_HEIGHT = 13;     // maximum height of map in cells
    public static final int COLUMN_WIDTH = 32;   // spacing between balls horizontally (pixels)
    public static final int ROW_HEIGHT = 28;     // spacing between rows of balls (pixels)

    private Cell[] cells = new Cell[MAX_WIDTH * MAX_HEIGHT]; // the map itself
    private int[] allowedBallTypes; // array of currently allowed ball types
    private boolean won; // set when the map has been cleared
    private World world; // just so we can call addObject in the loadMap method.

    public Map(World w, String map)
    {
        world = w;

        // Initialize the array.
        for(int i = 0; i < cells.length; ++i)
        {
            cells[i] = new Cell(null, true);
        }

        // Load the map itself.
        loadMap(map);
    }

    public void update()
    {
        // Look for detached balls and make them fall off the map.
        updateBalls();

        // Update the array of allowed ball types, because we should only
        // let the cannon spawn ball types that exist in the map right now.
        updateAllowedBallTypes();

    }

    public boolean hasWon()
    {
        return won;
    }

    public int[] getAllowedBallTypes()
    {
        return allowedBallTypes;
    }

    // Return the number of hex rows in the map.
    public int getCellCountY()
    {
        return MAX_HEIGHT;
    }

    // Return the length of a given hex row.   
    public int getCellCountX(int j)
    {
        boolean odd = (j & 1) == 1;
        return odd ? MAX_WIDTH-1 : MAX_WIDTH;
    }

    // Return the x screen position for the given hex coordinates.
    public int getX(int i, int j)
    {
        boolean odd = (j & 1) == 1;
        return 16 + i * COLUMN_WIDTH + (odd ? 16 : 0);
    }

    // Return the y screen position for the given hex coordinates.
    public int getY(int i, int j)
    {
        return 16 + j * ROW_HEIGHT;
    }

    // Put a ball at a given set of hex coordinates. 
    // Also check if it matches any neighboring balls, and make them fall if so.
    public void setBall(int i, int j, Ball ball)
    {
        Cell c = getCell(i,j);

        if(c == null || c.getBall() != null)
        {
            return;
        }

        c.setBall(ball);

        handleMatches(i,j);
    }

    // Return the map entry for the given hex coordinates, or null.
    public Cell getCell(int i, int j)
    {
        if(i < 0 || j < 0 || i >= getCellCountX(j) || j >= getCellCountY())
        {
            return null;
        }

        return cells[i + j * MAX_WIDTH];
    }

    // Load a map and spawn balls etc.
    private void loadMap(String map)
    {
        // Lookup table for the string-based maps.
        String lookups = "0123456789 "; 
        
        // Clear out spaces.
        map = map.replaceAll(" ","");

        // Loop over how many balls or spaces are expected.
        int ci = 0;
        for(int j = 0; j < getCellCountY(); ++j)
        {
            for(int i = 0; i < getCellCountX(j); ++i)
            {
                // Stop if the map text ends.
                if(ci >= map.length())
                {
                    return;
                }

                // Translate the character in the map string to a ball type.
                int type = lookups.indexOf(map.charAt(ci++));

                // If it's a valid type, spawn a ball.
                if(type >= 0 && type < Ball.typeCount)
                {
                    // Add the ball to the world.
                    Ball ball = new Ball(type);
                    world.addObject(ball, getX(i,j), getY(i,j));

                    // And to the map. (Don't use setBall(i,j,ball) as this will cause matches!)
                    getCell(i,j).setBall(ball);
                }
            }
        }
    }

    // Given the position of a new ball, find matching ones and make them fall off the playfield.
    private void handleMatches(int i, int j)
    {
        // Generate a list of matching balls.
        ArrayList<Cell> matches = new ArrayList<Cell>();
        
        // Recursively find matching balls among the neighbors.
        match(i,j,matches);

        // If we got >= 3 matches, clear them!
        if(matches.size() >= 3)
        {
            for(Cell c : matches)
            {
                world.addObject(new StickEffect(), c.getBall().getX(), c.getBall().getY());
                c.getBall().fall();
                c.setBall(null);
            }
        }
    }

    // Recursively match a ball and same-colored neighbors.
    private void match(int i, int j, ArrayList<Cell> matches)
    {
        Cell c = getCell(i,j);

        // Skip empty space or space outside map.
        // And don't add one that's already been matched.
        if(c == null || c.getBall() == null || matches.contains(c))
        {
            return;
        }

        // Match this ball if it has the same type as the ones already matched.
        if(matches.isEmpty() || c.getBall().getType() == matches.get(0).getBall().getType())
        {
            // Record it.
            matches.add(c);

            // Left/right neighbors.
            match(i-1, j, matches);
            match(i+1, j, matches);

            // Odd and even rows have their diagonal neighbors positioned differently in i,j coordinates.
            if((j & 1) == 1) // odd row
            {
                match(i, j-1, matches);
                match(i+1, j-1, matches);
                match(i, j+1, matches);
                match(i+1, j+1, matches);
            }
            else // even row
            {
                match(i-1, j-1, matches);
                match(i, j-1, matches);
                match(i-1, j+1, matches);
                match(i, j+1, matches);
            }
        }
    }

    // Update the 'allowedBallTypes' array based on what is in the map.
    // Note: This is complicated but avoids O(n*m) search.
    public void updateAllowedBallTypes()
    {
        int allowedCount = 0;
        boolean[] allowed = new boolean[Ball.typeCount];
        
        // Only ball types that exist in the map RIGHT NOW as attached balls will be allowed.
        for(Cell c : cells)
        {
            if(c != null && c.getBall() != null && c.isAttached())
            {
                int type = c.getBall().getType();
                
                if(!allowed[type])
                    allowedCount++;
                
                allowed[type] = true;
            }
        }
        
        allowedBallTypes = new int[allowedCount];
        int writeIndex = 0;
        for(int type = 0; type < Ball.typeCount; ++type)
        {
            if(allowed[type])
            {
                allowedBallTypes[writeIndex++] = type;
            }
        }
    }

    private void updateBalls()
    {
        // Mark all balls as unattached.
        for(Cell c : cells)
        {
            c.setAttached(false);
        }

        // Find the balls attached to the top edge of the map, and scan ALL their neighbors recursively.
        // This will cause a flood fill of the "attached" flag. Any balls NOT reached by this fill
        // should be cleared away.
        for(int i = 0; i < getCellCountX(0); ++i)
        {
            Cell c = cells[i];

            if(c.getBall() != null)
            {
                markAttached(i,0);
            }
        }

        // Clear any balls that don't have the attached flag set.
        // ALSO set the 'won' attribute if the game was won (== no attached balls on the map).
        won = true;
        for(Cell c : cells)
        {
            if(c != null && c.getBall() != null)
            {
                if(c.isAttached())
                {
                    won = false;
                }
                else
                {
                    c.getBall().fall();
                    c.setBall(null);
                }
            }
        }
    }

    private void markAttached(int i, int j)
    {
        Cell c = getCell(i,j);

        // Don't continue with:
        // 1) Invalid cells 
        // 2) Empty cells
        // 3) Already attached cells
        if(c == null || c.getBall() == null || c.isAttached())
        {
            return;
        }

        c.setAttached(true);

        // Left/right neighbors.
        markAttached(i-1, j);
        markAttached(i+1, j);

        // Odd and even rows have their diagonal neighbors positioned differently in i,j coordinates.
        if((j & 1) == 1) // odd row
        {
            markAttached(i, j-1);
            markAttached(i+1, j-1);
            markAttached(i, j+1);
            markAttached(i+1, j+1);
        }
        else // even row
        {
            markAttached(i-1, j-1);
            markAttached(i, j-1);
            markAttached(i-1, j+1);
            markAttached(i, j+1);
        }
    }

    // Return the nearest free space to the specified pixel coordinates.
    public Position findFreeCell(int x, int y)
    {
        // Linear search through the map, just return the closest map entry position.
        // This could be more efficient.
        double minDistance = Double.MAX_VALUE;
        Position pos = null;

        for(int j = 0; j < getCellCountY(); ++j)
        {
            for(int i = 0; i < getCellCountX(j); ++i)
            {
                Cell c = getCell(i,j);

                // Only consider empty cells.
                if(c != null && c.getBall() == null)
                {
                    double dx = getX(i,j) - x;
                    double dy = getY(i,j) - y;
                    double distance = dx*dx+dy*dy;

                    if(distance < minDistance)
                    {
                        minDistance = distance;

                        if(pos == null)
                        {
                            pos = new Position();
                        }

                        pos.i = i;
                        pos.j = j;
                    }
                }
            }
        }

        // Return the position (or null if none found)
        return pos;
    }

}
