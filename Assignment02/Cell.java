/**
 * This represents a "cell" in the hex grid of the map.
 * 
 * @author Carl Ådahl
 * @version 2010-10
 */

public class Cell
{
    public Cell(Ball ball, boolean attached)
    {
        _ball = ball;
        _attached = attached;
    }
    
    public Ball getBall()
    {
        return _ball;
    }

    public void setBall(Ball ball)
    {
        _ball = ball;
    }

    public boolean isAttached()
    {
        return _attached;
    }

    public void setAttached(boolean attached)
    {
        _attached = attached;
    }

    private Ball _ball;
    private boolean _attached;
}
