import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;

/**
 * The ball object.
 * 
 * @author Carl Ådahl
 * @version 2010-10
 */
public class Ball extends SmoothActor
{
    public static final int typeCount = 8; // there are eight ball colors

    // List of possible balls. This is static, so they are only loaded once from disk.
    private static GreenfootImage[] images = 
        {
            new GreenfootImage("bubble-1.png"),
            new GreenfootImage("bubble-2.png"),
            new GreenfootImage("bubble-3.png"),
            new GreenfootImage("bubble-4.png"),
            new GreenfootImage("bubble-5.png"),
            new GreenfootImage("bubble-6.png"),
            new GreenfootImage("bubble-7.png"),
            new GreenfootImage("bubble-8.png")
        };

    private static final double MOVE_SPEED = 6; // speed when shot from the cannon
    private static final int FALL_HSPEED = 3; // horizontal speed (random max)
    private static final int FALL_VSPEED = 10; // initial speed upwards when "falling" (random max)
    private static final double FALL_GRAVITY = 0.12; // gravity acceleration added to the v. speed every act().

    // The three states a ball can be in.
    public static final int STATE_MOVING = 0;    // ball is currently moving
    public static final int STATE_STUCK = 1;     // ball is currently stuck on the map
    public static final int STATE_FALLING = 2;   // ball is falling off the map

    private int type; // color of ball
    private int state = STATE_STUCK; // current state.
    private double exactX, exactY; // floating point position for smoothness.
    private double velocityX, velocityY; // floating point velocity.

    public Ball(int t)
    {
        type = t;
        
        // Set the correct image based on the type (color).
        setImage(images[type]);
    }

    protected void addedToWorld(World world)
    {
        // Store the initial position.
        exactX = getX();
        exactY = getY();
    }

    public int getType()
    {
        return type;
    }

    public int getState()
    {
        return state;
    }

    // stateMoving: Begin moving along the specified angle.
    public void move(double angle)
    {
        state = STATE_MOVING;
        double a = (angle + 270) * Math.PI / 180.0;
        velocityX = Math.cos(a) * MOVE_SPEED;
        velocityY = Math.sin(a) * MOVE_SPEED;
    }

    // stateStuck: Begin a quiet life just stuck to the map.
    private void stick()
    {
        state = STATE_STUCK;
        
        // Make sure the ball doesn't keep moving.
        velocityX = 0;
        velocityY = 0;
        
        // Ask the map for a free cell near our position.
        Map map = ((BubbleWorld)getWorld()).getMap();
        Map.Position pos = map.findFreeCell((int)exactX, (int)exactY);
        
        // Set the adjusted x, y.
        exactX = map.getX(pos.i, pos.j);
        exactY = map.getY(pos.i, pos.j);

        // Associate the ball with the free map cell.
        map.setBall(pos.i, pos.j, this);

        // Spawn an effect.
        getWorld().addObject(new StickEffect(), (int)exactX, (int)exactY);
        
        // Play sound when the ball sticks to something.
        Greenfoot.playSound("stick.wav");
    }

    // stateFalling: Begin to fall off the map.
    public void fall()
    {
        state = STATE_FALLING;

        // Set up initial velocities.
        velocityX += (Greenfoot.getRandomNumber(FALL_HSPEED)/10.0+0.5) * (Greenfoot.getRandomNumber(2)*2-1);
        velocityY -= (Greenfoot.getRandomNumber(FALL_VSPEED)/10.0+0.5);
    }

    private void checkCollisions()
    {
        int left = 16;
        int right = getWorld().getWidth()-16;

        if(exactX < left)
        {
            exactX -= velocityX*2;
            velocityX = -velocityX;
            
            // Play sound when the ball bounces off a wall.
            Greenfoot.playSound("bounce.wav");
        }
        else if(exactX >= right)
        {
            exactX -= velocityX*2;
            velocityX = -velocityX;
            
            // Play sound when the ball bounces off a wall.
            Greenfoot.playSound("bounce.wav");
        }

        if(exactY < 16)
        {
            stick();
        }
        else
        {
            List<Ball> others = getIntersectingObjects(Ball.class);
            double collisionDistance = Math.pow(32 * 0.82, 2);
            boolean tooClose = false;
            for(Ball b : others)
            {
                double dx = b.getX() - getX();
                double dy = b.getY() - getY();
                double d2 = dx*dx + dy*dy;

                if( b.getState() == Ball.STATE_STUCK &&
                    d2 <= collisionDistance)
                {
                    tooClose = true;
                }
            }

            if(tooClose)
            {
                stick();
            }

        }
    }

    public void act(double deltaTime) 
    {
        if(state == STATE_MOVING)
        {
            checkCollisions();

            exactX += velocityX * deltaTime;
            exactY += velocityY * deltaTime;
            setLocation((int)exactX, (int)exactY);   
        }
        else if(state == STATE_FALLING)
        {
            velocityY += FALL_GRAVITY * deltaTime;
            exactX += velocityX * deltaTime;
            exactY += velocityY * deltaTime;
            setLocation((int)exactX, (int)exactY);   

            if(exactY >= getWorld().getHeight()+getImage().getHeight()/2)
            {
                getWorld().removeObject(this);
            }
        }

    }    
}
