import greenfoot.*;
import java.awt.Color;

public class Cell extends Actor {
    
    public boolean temp;
    private boolean alive;
    private int size, grayScale, transparency = 255;
    private GreenfootImage image;
    
    public Cell(int cellSize)
    {
        alive = false;
        size = cellSize;
        grayScale = 0;
        
        image = new GreenfootImage(size, size);
        image.setColor(new Color(0, 0, 0, transparency));
        image.fillRect(0, 0, size, size);
        setImage(image);
    }
    
    public void populate()
    {
        alive = true;
        grayScale = 255;
        
        image.setColor(new Color(0, grayScale, 0, transparency));
        image.fillRect(0, 0, size, size);
    }
    
    public void kill()
    {
        alive = false;
        grayScale = 0;
        
        image.setColor(new Color(0, grayScale, 0, transparency));
        image.fillRect(0, 0, size, size);
    }
    
    public void renderGrayScale(int value)
    {
        grayScale += value;
        if (grayScale < 0)
            grayScale = 0;
        if (grayScale > 255)
            grayScale = 255;
        
        image.setColor(new Color(0, grayScale, 0, transparency));
        image.fillRect(0, 0, size, size);
        
    }
    
    public boolean isAlive()
    {
        return alive;
    }
    
    public boolean setAlive(boolean value)
    {
        alive = value;
        return alive;
    }
    
}
