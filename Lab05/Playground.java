import greenfoot.*;

public class Playground extends World {
    
    private int size, cellSize, timeStep = 0;
    private Cell[][] cell;
    
    public Playground()
    {
        super(50, 50, 10);
        size = getWidth();
        cellSize = getCellSize();
        
        cell = new Cell[size][size];
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                cell[x][y] = new Cell(cellSize);
                addObject(cell[x][y], x, y);
            }
        }
        
        // populate cells randomly
        for (int i = 0; i < size * size / 5;) {
            int x = Greenfoot.getRandomNumber(size);
            int y = Greenfoot.getRandomNumber(size);
            if (!cell[x][y].isAlive()) {
                cell[x][y].populate();
                i++;
            }
        }
        
    }
    
    public void act()
    {
        if (timeStep == 63) {
            /* generate the next generation of cells
             * in every 1/64 time period */
            timeStep = 0;
            
            for (int x = 0; x < size; x++) {
                for (int y = 0; y < size; y++) {
                    int num = getNeighborCellNum(x, y);
                    if (num < 2 || num > 3)
                        cell[x][y].temp = false;
                    else if (num == 3)
                        cell[x][y].temp = true;
                }
            }
            
            // synchronize
            for (int x = 0; x < size; x++) {
                for (int y = 0; y < size; y++)
                    cell[x][y].setAlive(cell[x][y].temp);
            }
            
        } else {
            /* gradually change the color of each cell
             * in every 63/64 time period */
            timeStep++;
            
            for (int x = 0; x < size; x++) {
                for (int y = 0; y < size; y++) {
                    if (cell[x][y].isAlive())
                        cell[x][y].renderGrayScale(+4);
                    else
                        cell[x][y].renderGrayScale(-4);
                    
                }
            }
            
        }
    }
    
    private int getNeighborCellNum(int x, int y)
    {
        int sum = 0;
        if (x - 1 >= 0 && y - 1 >= 0)
            if (cell[x - 1][y - 1].isAlive())
                sum++;
        if (x - 1 >= 0)
            if (cell[x - 1][y].isAlive())
                sum++;
        if (x - 1 >= 0 && y + 1 < size)
            if (cell[x - 1][y + 1].isAlive())
                sum++;
        if (y - 1 >= 0)
            if (cell[x][y - 1].isAlive())
                sum++;
        if (y + 1 < size)
            if (cell[x][y + 1].isAlive())
                sum++;
        if (x + 1 < size && y - 1 >= 0)
            if (cell[x + 1][y - 1].isAlive())
                sum++;
        if (x + 1 < size)
            if (cell[x + 1][y].isAlive())
                sum++;
        if (x + 1 < size && y + 1 < size)
            if (cell[x + 1][y + 1].isAlive())
                sum++;
        return sum;
    }
    
}
