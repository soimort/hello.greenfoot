import greenfoot.*;

public class Playground extends World {
    public Snake[] snake;
    public God god;
    
    private int maxSegmentNum = 40;
    
    public Playground()
    {
        super(400, 400, 1);
        
        Greenfoot.setSpeed(30);
        
        // add the snake
        snake = new Snake[maxSegmentNum];
        snake[0] = new Snake("head");
        snake[1] = new Snake("body");
        snake[2] = new Snake("body");
        addObject(snake[2], 30, 30);
        addObject(snake[1], 50, 30);
        addObject(snake[0], 70, 30);
        
        // add the wall
        addObject(new Brick(), 70, 290);
        addObject(new Brick(), 70, 310);
        addObject(new Brick(), 90, 290);
        addObject(new Brick(), 90, 310);
        addObject(new Brick(), 150, 150);
        addObject(new Brick(), 150, 170);
        addObject(new Brick(), 150, 190);
        addObject(new Brick(), 150, 210);
        addObject(new Brick(), 170, 210);
        addObject(new Brick(), 190, 210);
        addObject(new Brick(), 210, 210);
        addObject(new Brick(), 230, 90);
        addObject(new Brick(), 250, 90);
        addObject(new Brick(), 270, 90);
        addObject(new Brick(), 290, 90);
        addObject(new Brick(), 290, 110);
        addObject(new Brick(), 290, 130);
        addObject(new Brick(), 290, 150);
        addObject(new Brick(), 290, 170);
        addObject(new Brick(), 290, 190);
        addObject(new Brick(), 290, 210);
        addObject(new Brick(), 290, 230);
        addObject(new Brick(), 290, 250);
        addObject(new Brick(), 290, 270);
        
        // add the god and some food
        god = new God();
        addObject(god, 0, 0);
        for (int i = 0; i < 5; i++)
            god.addFoodRandomly();
        
    }
}
