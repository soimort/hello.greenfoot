import greenfoot.*;

public class God extends Actor {
    public God()
    {
    }
    
    public void act()
    {
    }
    
    public void addFoodRandomly()
    {
        int x = (Greenfoot.getRandomNumber(18) + 1) * 20 + 10;
        int y = (Greenfoot.getRandomNumber(18) + 1) * 20 + 10;
        while (!getWorld().getObjectsAt(x, y, Actor.class).isEmpty()) {
            x = (Greenfoot.getRandomNumber(18) + 1) * 20 + 10;
            y = (Greenfoot.getRandomNumber(18) + 1) * 20 + 10;
        }
        addFood(x, y);
    }
    
    private void addFood(int x, int y)
    {
        getWorld().addObject(new Food(), x, y);
    }
}
