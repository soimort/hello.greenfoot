import greenfoot.*;
import java.awt.Color;
import java.awt.Font;
import java.util.Calendar;

public class ScoreBoard extends Actor {
    public static final float fontSize = 15.0f;
    public static final int width = 120;
    public static final int height = 60;
    
    public ScoreBoard(int score)
    {
        GreenfootImage image = new GreenfootImage(width, height);
        
        image.setColor(new Color(255, 255, 255, 128));
        image.fillRect(0, 0, width, height);
        image.setColor(new Color(0, 0, 0, 128));
        image.fillRect(5, 5, width - 10, height - 10);
        Font font = image.getFont();
        font = font.deriveFont(fontSize);
        image.setFont(font);
        image.setColor(Color.WHITE);
        image.drawString("GAME OVER", 10, 20);
        image.drawString("SCORE: " + Integer.toString(score), 10, 40);
        setImage(image);
    }
    
}
