import greenfoot.*;

public class Snake extends Actor {
    private String segment, direction;
    private int speed, speedX, speedY, length;
    
    public Snake(String segment)
    {
        this.segment = segment;
        direction = "right";
        speedX = speed = 20;
        speedY = 0;
        length = 3;
    }
    
    public void act()
    {
        if (segment == "head") {
            Playground playground = (Playground)getWorld();
            for (int i = length - 1; i > 0; i--)
                playground.snake[i].setLocation(playground.snake[i - 1].getX(), playground.snake[i - 1].getY());
            
            keyboardControl();
            move();
            eat();
        }
    }
    
    private void keyboardControl()
    {
        if (Greenfoot.isKeyDown("left") && direction != "right") {
            speedX = -speed;
            speedY = 0;
            direction = "left";
            
        } else if (Greenfoot.isKeyDown("right") && direction != "left") {
            speedX = speed;
            speedY = 0;
            direction = "right";
            
        } else if (Greenfoot.isKeyDown("up") && direction != "down") {
            speedX = 0;
            speedY = -speed;
            direction = "up";
            
        } else if (Greenfoot.isKeyDown("down") && direction != "up") {
            speedX = 0;
            speedY = speed;
            direction = "down";
            
        }
    }
    
    private void move()
    {
        // check if hit the edge of screen
        if (getX() + speedX < 0 || getX() + speedX >= 400
            || getY() + speedY < 0 || getY() + speedY >= 400)
            die();
        
        // check if hit the wall
        Actor brick = getOneObjectAtOffset(speedX, speedY, Brick.class);
        if (brick != null)
            die();
        
        // check if hit the body
        Actor body = getOneObjectAtOffset(speedX * 2, speedY * 2, Snake.class);
        if (body != null)
            die();
        
        setLocation(getX() + speedX, getY() + speedY);
    }
    
    private void eat()
    {
        Actor food = getOneObjectAtOffset(0, 0, Food.class);
        if (food != null) {
            Greenfoot.playSound("snakeeat.mp3");
            
            Playground playground = (Playground)getWorld();
            playground.removeObject(food);
            playground.snake[length] = new Snake("body");
            playground.addObject(playground.snake[length], playground.snake[length - 1].getX(), playground.snake[length - 1].getY());
            
            length++;
            
            // add food randomly
            playground.god.addFoodRandomly();
        }
    }
    
    private void die()
    {
        Greenfoot.playSound("snakedie.wav");
        
        getWorld().addObject(new ScoreBoard(length), getWorld().getWidth() / 2, getWorld().getHeight() / 2);
        
        Greenfoot.stop();
    }
}
