import greenfoot.*;

public class Tux extends Actor
{
    public Tux()
    {
        speed = 2;
        velocity = new TuxVelocity();
    }
    
    // Action frequently executed by Actor
    public void act()
    {
        checkKeyPress();
        move();
    }
    
    private void checkKeyPress()
    {
        if (Greenfoot.isKeyDown("left") || Greenfoot.isKeyDown("a"))
            velocity.setXY(-speed, 0);
        else if (Greenfoot.isKeyDown("right") || Greenfoot.isKeyDown("d"))
            velocity.setXY(speed, 0);
        else if (Greenfoot.isKeyDown("up") || Greenfoot.isKeyDown("w"))
            velocity.setXY(0, -speed);
        else if (Greenfoot.isKeyDown("down") || Greenfoot.isKeyDown("s"))
            velocity.setXY(0, speed);
        else if (Greenfoot.isKeyDown("space"))
            velocity.setXY(0, 0);
    }
    
    private void move()
    {
        setLocation(getX() + velocity.getX(), getY() + velocity.getY());
    }
    
    private int speed;
    private TuxVelocity velocity;
    
}
