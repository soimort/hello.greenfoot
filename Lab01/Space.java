import greenfoot.*;

public class Space extends World
{
    public Space()
    {
        super(500, 500, 1);
        
        Tux tux = new Tux();
        addObject(tux, getWidth() / 2, getHeight() / 2);
    }
}
