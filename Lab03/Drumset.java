import greenfoot.*;

public class Drumset extends World {

    public Tone tone[];
    public int toneNum;
    
    public Drumset()
    {
        super(600, 400, 1);
        
        toneNum = 10;
        tone = new Tone[toneNum];
        tone[0] = new Tone("drum4.wav", "r", 80, 10, 185, 120);
        tone[1] = new Tone("drum7.wav", "space", 185, 90, 260, 155);
        tone[2] = new Tone("drum4.wav", "u", 260, 10, 350, 110);
        tone[3] = new Tone("drum6.wav", "i", 350, 110, 455, 210);
        tone[4] = new Tone("drum8.wav", "o", 455, 150, 555, 250);
        tone[5] = new Tone("drum2.wav", "d", 25, 170, 100, 260);
        tone[6] = new Tone("drum1.wav", "f", 115, 160, 195, 240);
        tone[7] = new Tone("drum0.wav", "j", 255, 160, 330, 260);
        tone[8] = new Tone("drum5.wav", "k", 330, 250, 420, 360);
        tone[9] = new Tone("drum6.wav", "l", 440, 265, 540, 375);
        
    }
    
    public void act()
    {
        checkKeyboard();
        checkMouse();
    }
    
    public void checkMouse()
    {
        if (Greenfoot.mouseClicked(this)) {
            MouseInfo mouse = Greenfoot.getMouseInfo();
            for (int i = 0; i < toneNum; i++) {
                if (mouse.getX() > tone[i].getMouseAreaLeft() && mouse.getX() < tone[i].getMouseAreaRight()
                    && mouse.getY() > tone[i].getMouseAreaTop() && mouse.getY() < tone[i].getMouseAreaBottom())
                    tone[i].play();
            }
        }
    }
    
    public void checkKeyboard()
    {
        for (int i = 0; i < toneNum; i++) {
            if (Greenfoot.isKeyDown(tone[i].getKeyBinding()))
                tone[i].play();
        }
    }
    
}
