import greenfoot.*;

public class Tone extends Actor {
    
    private String soundFile, keyBinding;
    private int mouseAreaLeft, mouseAreaTop, mouseAreaRight, mouseAreaBottom;
    
    public Tone(String soundFile, String keyBinding, int mouseAreaLeft, int mouseAreaTop, int mouseAreaRight, int mouseAreaBottom) {
        this.soundFile = soundFile;
        this.keyBinding = keyBinding;
        this.mouseAreaLeft = mouseAreaLeft;
        this.mouseAreaTop = mouseAreaTop;
        this.mouseAreaRight = mouseAreaRight;
        this.mouseAreaBottom = mouseAreaBottom;
        
    }
    
    public void act()
    {
    }
    
    public void play()
    {
        Greenfoot.playSound(soundFile);
    }
    
    public String getKeyBinding()
    {
        return keyBinding;
    }
    
    public int getMouseAreaLeft()
    {
        return mouseAreaLeft;
    }
    
    public int getMouseAreaTop()
    {
        return mouseAreaTop;
    }
    
    public int getMouseAreaRight()
    {
        return mouseAreaRight;
    }
    
    public int getMouseAreaBottom()
    {
        return mouseAreaBottom;
    }
    
}
