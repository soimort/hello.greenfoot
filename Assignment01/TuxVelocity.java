public class TuxVelocity {
    private int x, y;
    
    public TuxVelocity()
    {
        x = y = 0;
    }
    
    public int getX()
    {
        return x;
    }
    
    public int getY()
    {
        return y;
    }
    
    public void setX(int xValue)
    {
        x = xValue;
    }
    
    public void setY(int yValue)
    {
        y = yValue;
    }
    
    public void setXY(int xValue, int yValue)
    {
        x = xValue;
        y = yValue;
    }
    
}
