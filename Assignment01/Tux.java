import greenfoot.*;
import java.util.List;

public class Tux extends Actor {
    public int energy, score, foodEaten, lobsterEaten, fishEaten, hamburgerEaten;
    public ScoreBoard scoreboard;
    private int speed;
    private TuxVelocity velocity;
    
    public Tux(ScoreBoard sb)
    {
        energy = 100;
        score = 0;
        scoreboard = sb;
        foodEaten = lobsterEaten = fishEaten = hamburgerEaten = 0;
        speed = 2;
        velocity = new TuxVelocity();
    }
    
    public void act()
    {
        if (Greenfoot.getRandomNumber(30) == 0)
            energy--;
        if (energy <= 0)
            die();
        
        if (foundFood())
            eatFood();
        if (foundBomb())
            getHurt();
        
        scoreboard.setDisplay(energy, score, lobsterEaten, fishEaten, hamburgerEaten);
        
        checkKeyPress();
        if (!foundObstacle())
            move();
    }
    
    private boolean foundFood()
    {
        List foods = getObjectsInRange(20, Food.class);
        if (!foods.isEmpty())
            return true;
        else
            return false;
    }
    
    private boolean foundBomb()
    {
        Actor bomb = getOneObjectAtOffset(0, 0, Bomb.class);
        if (bomb != null)
            return true;
        else
            return false;
    }
    
    private boolean foundObstacle()
    {
        Actor obstacle = getOneObjectAtOffset(velocity.getX(), velocity.getY(), Obstacle.class);
        if (obstacle != null)
            return true;
        else
            return false;
    }
    
    private void eatFood()
    {
        List foods = getObjectsInRange(20, Food.class);
        Actor food = (greenfoot.Actor)foods.get(0);
        if (food != null) {
            foodEaten++;
            if (food.getClass() == Lobster.class) {
                lobsterEaten++;
                energy += 5;
                score += 50;
            } else if (food.getClass() == Fish.class) {
                fishEaten++;
                energy += 10;
                score += 100;
            } else if (food.getClass() == Hamburger.class) {
                hamburgerEaten++;
                energy += 20;
                score += 200;
            }
            if (energy > 100)
                energy = 100;
            
            getWorld().removeObject(food);
        }
    }
    
    private void getHurt()
    {
        Actor bomb = getOneObjectAtOffset(0, 0, Bomb.class);
        if (bomb != null) {
            Greenfoot.playSound("explosion.wav");
            
            energy -= 20;
            if (energy < 0)
                energy = 0;
            
            getWorld().removeObject(bomb);
        }
    }
    
    private void checkKeyPress()
    {
        if (Greenfoot.isKeyDown("left") || Greenfoot.isKeyDown("a"))
            velocity.setXY(-speed, 0);
        else if (Greenfoot.isKeyDown("right") || Greenfoot.isKeyDown("d"))
            velocity.setXY(speed, 0);
        else if (Greenfoot.isKeyDown("up") || Greenfoot.isKeyDown("w"))
            velocity.setXY(0, -speed);
        else if (Greenfoot.isKeyDown("down") || Greenfoot.isKeyDown("s"))
            velocity.setXY(0, speed);
        else if (Greenfoot.isKeyDown("space"))
            velocity.setXY(0, 0);
    }
    
    private void move()
    {
        setLocation(getX() + velocity.getX(), getY() + velocity.getY());
    }
    
    private void die()
    {
        getWorld().addObject(new ScoreBoard("GAME OVER", score), getWorld().getWidth() / 2, getWorld().getHeight() / 2);
        Greenfoot.stop();
    }
}
