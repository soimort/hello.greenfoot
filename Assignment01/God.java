import greenfoot.*;

public class God extends Actor {
    public void act()
    {
        int x = Greenfoot.getRandomNumber(getWorld().getWidth()), y = Greenfoot.getRandomNumber(getWorld().getHeight());
        if (getWorld().getObjectsAt(x, y, Actor.class).isEmpty()) {
            switch (Greenfoot.getRandomNumber(800)) {
            case 0: case 4:
                getWorld().addObject(new Bomb(), x, y);
                break;
            case 1: case 5: case 9:
                getWorld().addObject(new Lobster(), x, y);
                break;
            case 2: case 6:
                getWorld().addObject(new Fish(), x, y);
                break;
            case 3:
                getWorld().addObject(new Hamburger(), x, y);
                break;
            }
        }
    }
}
