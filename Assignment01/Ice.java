import greenfoot.*;

public class Ice extends World {
    public Tux tux;
    public ScoreBoard scoreboard;
    
    public Ice()
    {
        super(600, 600, 1);
        
        addObject(new God(), 100, 20);
        
        scoreboard = new ScoreBoard();
        tux = new Tux(scoreboard);
        
        for (int i = 0; i < 10;) {
            int x = Greenfoot.getRandomNumber(getWidth()), y = Greenfoot.getRandomNumber(getHeight());
            if (getObjectsAt(x, y, Actor.class).isEmpty()) {
                addObject(new Igloo(), x, y);
                i++;
            }
        }
        
        for (int i = 0; i < 10;) {
            int x = Greenfoot.getRandomNumber(getWidth()), y = Greenfoot.getRandomNumber(getHeight());
            if (getObjectsAt(x, y, Actor.class).isEmpty()) {
                switch (Greenfoot.getRandomNumber(4)) {
                case 0:
                    addObject(new Bomb(), x, y);
                    break;
                case 1:
                    addObject(new Lobster(), x, y);
                    break;
                case 2:
                    addObject(new Fish(), x, y);
                    break;
                case 3:
                    addObject(new Hamburger(), x, y);
                    break;
                }
                i++;
            }
        }
        
        for (int i = 0; i < 1;) {
            int x = getWidth() / 3 + Greenfoot.getRandomNumber(getWidth() / 3), y = getHeight() / 3 + Greenfoot.getRandomNumber(getHeight() / 3);
            if (getObjectsAt(x, y, Actor.class).isEmpty()) {
                addObject(tux, x, y);
                i++;
            }
        }
        
        scoreboard.setDisplay(100, 0, 0, 0, 0);
        addObject(scoreboard, 60, 50);
    }
}
