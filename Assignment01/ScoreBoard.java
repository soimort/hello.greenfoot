import greenfoot.*;
import java.awt.Color;
import java.awt.Font;
import java.util.Calendar;

public class ScoreBoard extends Actor {
    public static final float fontSize = 12.0f;
    public static final int width = 120;
    public static final int height = 100;
    
    public ScoreBoard()
    {
        setDisplay(100, 0, 0, 0, 0);
    }
    
    public ScoreBoard(String message, int score)
    {
        GreenfootImage image = new GreenfootImage(width, height);

        image.setColor(new Color(255, 255, 255, 128));
        image.fillRect(0, 0, width, height);
        image.setColor(new Color(0, 0, 0, 128));
        image.fillRect(5, 5, width - 10, height - 10);
        Font font = image.getFont();
        font = font.deriveFont(fontSize);
        image.setFont(font);
        image.setColor(Color.WHITE);
        image.drawString(message, 20, 50);
        image.drawString("SCORE: " + score, 20, 65);
        setImage(image);
    }
    
    public void setDisplay(int energy, int score, int lobsterEaten, int fishEaten, int hamburgerEaten)
    {
        GreenfootImage image = new GreenfootImage(width, height);

        image.setColor(new Color(255, 255, 255, 128));
        image.fillRect(0, 0, width, height);
        image.setColor(new Color(0, 0, 0, 128));
        image.fillRect(5, 5, width - 10, height - 10);
        Font font = image.getFont();
        font = font.deriveFont(fontSize);
        image.setFont(font);
        image.setColor(Color.WHITE);
        image.drawString("Score: " + score, 10, 20);
        image.drawString("* " + lobsterEaten + " lobsters", 10, 35);
        image.drawString("* " + fishEaten + " fish", 10, 50);
        image.drawString("* " + hamburgerEaten + " hamburgers", 10, 65);
        image.drawString("Energy: " + energy, 10, 85);
        setImage(image);
    }
    
    public void act()
    {
    }
    
}
