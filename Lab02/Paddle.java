import greenfoot.*;

public class Paddle extends Actor {
    private int width, height, speed;
    
    public Paddle()
    {
        width = 140;
        height = 20;
        speed = 3;
    }
    
    public void act() 
    {
        checkKeyPress();
    }
    
    public int getHeight()
    {
        return height;
    }
    
    public int getWidth()
    {
        return width;
    }
    
    private void checkKeyPress()
    {
        if (Greenfoot.isKeyDown("left")
            && getX() - width / 2 >= 0)
            setLocation(getX() - speed, getY());
        if (Greenfoot.isKeyDown("right")
            && getX() + width / 2 <= getWorld().getWidth() - 1)
            setLocation(getX() + speed, getY());
        
    }
}
