import greenfoot.*;
import java.awt.Color;
import java.awt.Font;
import java.util.Calendar;

public class ScoreBoard extends Actor {
    public static final float fontSize = 12.0f;
    public static final int width = 150;
    public static final int height = 80;
    
    public ScoreBoard()
    {
    }
    
    public ScoreBoard(String msg1, String msg2, String msg3, String msg4)
    {
        show(msg1, msg2, msg3, msg4);
    }
    
    public void show(String msg1, String msg2, String msg3, String msg4)
    {
        GreenfootImage image = new GreenfootImage(width, height);
        
        image.setColor(new Color(255, 255, 255, 128));
        image.fillRect(0, 0, width, height);
        image.setColor(new Color(0, 0, 0, 128));
        image.fillRect(5, 5, width - 10, height - 10);
        Font font = image.getFont();
        font = font.deriveFont(fontSize);
        image.setFont(font);
        image.setColor(Color.WHITE);
        image.drawString(msg1, 10, 20);
        image.drawString(msg2, 10, 40);
        image.drawString(msg3, 10, 55);
        image.drawString(msg4, 10, 70);
        setImage(image);
    }
    
    public void act()
    {
    }
    
}
