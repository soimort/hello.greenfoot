import greenfoot.*;

public class Box extends World {
    private ScoreBoard scoreboard;
    private Paddle paddle;
    
    public Box()
    {
        super(500, 600, 1);
        
        paddle = new Paddle();
        addObject(paddle, getWidth() / 2, getHeight() - 50);
        
        addObject(new Brick(), 100, 100);
        addObject(new Brick(), 200, 100);
        addObject(new Brick(), 300, 100);
        addObject(new Brick(), 400, 100);
        addObject(new Brick(), 150, 200);
        addObject(new Brick(), 350, 200);
        
        addObject(new Ball(), getWidth() / 2, getHeight() - 70);
        
        scoreboard = new ScoreBoard();
        addObject(scoreboard, 80, 45);
        
    }
    
    public Paddle getPaddle()
    {
        return paddle;
    }
    
    public ScoreBoard getScoreBoard()
    {
        return scoreboard;
    }
}
