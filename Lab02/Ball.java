import greenfoot.*;
import java.lang.Math.*;

public class Ball extends Actor {
    private int speedX, speedY, radius, sum, score;
    
    public Ball()
    {
        speedX = speedY = 5;
        radius = 8;
        sum = 6;
        score = 0;
    }
    
    public void act()
    {
        move();
    }
    
    private void move()
    {
        edgeDetection();
        
        paddleDetection();
        
        brickDetection();
        
        setLocation(getX() + speedX, getY() + speedY);
        
        refreshScoreBoard();
    }
    
    // Collision detection: the edge
    private void edgeDetection()
    {
        if (getX() - radius <= 0) {
            speedX = +Math.abs(speedX);
            Greenfoot.playSound("bounce.wav");
        } else if (getX() + radius >= getWorld().getWidth() - 1) {
            speedX = -Math.abs(speedX);
            Greenfoot.playSound("bounce.wav");
        } else if (getY() - radius <= 0) {
            speedY = +Math.abs(speedY);
            Greenfoot.playSound("bounce.wav");
        } else if (getY() + radius >= getWorld().getHeight() - 1) {
            getWorld().addObject(new ScoreBoard("GAME OVER", "", "Score: " + score, ""), getWorld().getWidth() / 2, getWorld().getHeight() / 2);
        Greenfoot.stop();
        }
    }
    
    // Collision detection: the paddle
    private void paddleDetection()
    {
        Box box = (Box) getWorld();
        Paddle paddle = box.getPaddle();
        
        if (getY() + radius >= paddle.getY() - paddle.getHeight() / 2
            && getX() >= paddle.getX() - paddle.getWidth() / 2
            && getX() <= paddle.getX() + paddle.getWidth() / 2) {
            speedY = -Math.abs(speedY);
            Greenfoot.playSound("bounce.wav");
        }
    }
    
    // Collision detection: the brick
    private void brickDetection()
    {
        Actor brick = getOneObjectAtOffset(radius, radius, Brick.class);
        if (brick != null) {
            Greenfoot.playSound("explosion.wav");
            getWorld().removeObject(brick);
            score += 100;
            sum --;
            if (sum == 0) {
                while (sum < 6) {
                    int x = Greenfoot.getRandomNumber(getWorld().getWidth()), y = Greenfoot.getRandomNumber(getWorld().getHeight() / 2);
                    if (getWorld().getObjectsAt(x, y, Actor.class).isEmpty()) {
                        getWorld().addObject(new Brick(), x, y);
                        sum++;
                    }
                }
            }
        }
    }
    
    private void refreshScoreBoard()
    {
        Box box = (Box) getWorld();
        ScoreBoard scoreboard = box.getScoreBoard();
        scoreboard.show("Score: " + score, "speedX=" + speedX, "speedY=" + speedY, "Location=(" + getX() + ", " + getY() + ")");
    }
}
